<?php

use App\Http\Controllers\BookController;
use App\Http\Controllers\BorrowController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/user', [UserController::class, 'index'])->name('home');

Route::get('/book', [BookController::class, 'index'])->name('book');
Route::get('/book/add', [BookController::class, 'create'])->name('book.add');
Route::post('/book/store', [BookController::class, 'store'])->name('book.store');
Route::get('/book/{id}/edit', [BookController::class, 'edit'])->name('book.edit');
Route::put('/book/{id}/update', [BookController::class, 'update'])->name('book.update');
Route::get('/book/{id}/delete', [BookController::class, 'destroy'])->name('book.delete');

Route::get('/borrow', [BorrowController::class, 'index'])->name('borrow');
Route::get('/borrow/add', [BorrowController::class, 'create'])->name('borrow.add');
Route::post('/borrow/store', [BorrowController::class, 'store'])->name('borrow.store');
Route::get('/borrow/{id}/edit', [BorrowController::class, 'edit'])->name('borrow.edit');
Route::put('/borrow/{id}/update', [BorrowController::class, 'update'])->name('borrow.update');
Route::get('/borrow/{id}/delete', [BorrowController::class, 'destroy'])->name('borrow.delete');

Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
