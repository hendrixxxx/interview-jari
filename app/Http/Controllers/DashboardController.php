<?php

namespace App\Http\Controllers;

use App\DataTables\BooksDataTable;
use App\Models\Book;
use App\Models\Borrow;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(BooksDataTable $dataTable)
    {
        $borrow = Borrow::whereYear('date', Carbon::now()->format('Y'))
            ->whereMonth('date', Carbon::now()->format('m'));
        $count_borrow_book_this_month = Book::JoinSub($borrow, 'borrow', function ($q) {
            $q->on('books.id', 'borrow.book_id');
        })->count();

        $borrow_sub_6_month = Borrow::whereBetween('date', [Carbon::now()->subMonths(6), Carbon::now()]);
        $count_borrow_sub_6_month = Book::JoinSub($borrow_sub_6_month, 'borrow', function ($q) {
            $q->on('books.id', 'borrow.book_id');
        })->count();

        $user_more_borrow_book = User::withCount('book')
            ->orderBy('book_count', 'desc')
            ->limit(5)
            ->get();
        $data = [
            'count_borrow_book_this_month' => $count_borrow_book_this_month,
            'count_borrow_sub_6_month' => $count_borrow_sub_6_month,
            'user_more_borrow_book' => $user_more_borrow_book,
        ];
        return $dataTable->render('dashboard.page', $data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('dashboard.add.page');
    }

    public function store(Request $request)
    {
        Book::create(['name' => $request->name]);
        return redirect()->route('book');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $data = ['book' => Book::find($id)];
        return view('dashboard.update.page', $data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update($id, Request $request)
    {
        $book = Book::find($id);
        $book->name = $request->name;
        $book->save();
        return redirect()->route('book');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        return redirect()->route('book');
    }
}
