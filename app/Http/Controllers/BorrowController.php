<?php

namespace App\Http\Controllers;

use App\DataTables\BorrowDataTable;
use App\Models\Book;
use App\Models\Borrow;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BorrowController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(BorrowDataTable $dataTable)
    {
        return $dataTable->render('borrow.page');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $data = [
            'book' => Book::pluck('name', 'id'),
            'user' => User::pluck('name', 'id'),
        ];
        return view('borrow.add.page', $data);
    }

    public function store(Request $request)
    {
        Borrow::create([
            'user_id' => $request->user,
            'book_id' => $request->book,
            'date' => Carbon::parse($request->date),
        ]);
        return redirect()->route('borrow');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $data = [
            'borrow' => Borrow::find($id),
            'book' => Book::pluck('name', 'id'),
            'user' => User::pluck('name', 'id'),
        ];
        return view('borrow.update.page', $data);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update($id, Request $request)
    {
        $borrow = Borrow::find($id);
        $borrow->book_id = $request->book;
        $borrow->user_id = $request->user;
        $borrow->date = Carbon::parse($request->date);
        $borrow->save();
        return redirect()->route('borrow');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy($id)
    {
        $borrow = Borrow::find($id);
        $borrow->delete();
        return redirect()->route('borrow');
    }
}
