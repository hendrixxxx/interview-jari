## How to run
1. make database with name `pinjam_buku`
2. ``` composer install ```
3. ``` php artisan migrate --seed ```
4. ``` npm run dev ```
5. ``` php artisan serve ```
